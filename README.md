Henfield Storage is family-owned and operates as part of a successful business group which has been running for over 40 years. All of our personal and business customers benefit from low cost storage at our bases in North & West London, South & Southwest London, Central London, Surrey and, Sussex.

Address: Unit 4, Shield Drive, Brentford, London TW8 9EX, UK

Phone: +44 20 8568 3561
